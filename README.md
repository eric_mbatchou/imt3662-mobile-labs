# IMT3662 labs #

This repo contains example code and labs for IMT3662 mobile development course. The projects use Android Studio 14, Android SDK 23 and the NDK 1.0. The repo has been updated last in October 2015. 


## Activity Transition Demo ##

First entry level application to demonstrate fundamentals of Android UI, basic concepts of Activity and Fragments, and how to manage the activity stack. You will learn here how to:

   * animating activity transitions
   * testing with JUnit
   * testing with Espresso


## Tiles Puzzle ##

This application demonstrates how to incorporate 3rd party library, how to use OpenCV manager to dynamically load shared libraries, and how to setup an Android Studio project from an existing Eclipse-style source tree. Demo for:

   * using external library dependencies
   * using OpenCV 3 
   * using Settings and SettingsActivity


## Hello JNI ##

This is a slightly modified version of the default HelloJNI demo shipped with the Android SDK. It showcases how to make a native method call into C from within Java, and how to pass primitive types from Java-world to C-world. 


## SFML example ##

This is a default project to test that your setup for NDK development with NativeActivities is correct. It uses [SFML](https://github.com/SFML/SFML) as the media layer, and C code to instantiate window and handle user events.