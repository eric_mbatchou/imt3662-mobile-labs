package com.hig.transitiondemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Second activity, responsible for showing prompt to the user,
 * and passing the entered name back to the calling activity.
 */
public class SecondActivity extends AppCompatActivity {

    TextView textRequest;
    EditText editName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // Setup the UI
        this.textRequest = (TextView) findViewById(R.id.text_request);
        this.editName = (EditText) findViewById(R.id.editName);

        // Initiate the request prompt, passed to us from the first activity
        final Intent i = getIntent();
        this.textRequest.setText(i.getStringExtra(MainActivity.REQUEST));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }


    @Override
    public void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }


    public void closeActivity(final View v) {
        final Intent intent = new Intent();
        final Bundle bundle = new Bundle();
        bundle.putString(MainActivity.RESPONSE, this.editName.getText().toString());
        intent.putExtras(bundle);
        this.setResult(RESULT_OK, intent);
        finish();
    }


}
