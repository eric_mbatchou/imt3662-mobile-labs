package com.hig.transitiondemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


/**
 * Main activity of this project.
 */
public class MainActivity extends AppCompatActivity {

    static final String REQUEST = "main.activity.request";
    static final String RESPONSE = "main.activity.response";
    static final int REQUEST_CODE = 1001;

    TextView textMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.textMessage = (TextView) findViewById(R.id.text_name);
    }


    /**
     * Launches the second activity.
     * @param v button that was pressed
     */
    public void launchSecondActivity(final View v) {
        final Intent i = new Intent(this, SecondActivity.class);

        // We dynamically get approriate string from res/values/strings.xml
        i.putExtra(REQUEST, getString(R.string.main_activity_request));

        startActivityForResult(i, REQUEST_CODE);

        // One could use Android predefined transitions, but these are somewhat limited:
        // overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        // We will use our own ones:
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) { // user has not pressed Back Button
                final String name = data.getStringExtra(RESPONSE);
                this.textMessage.setText(generateHelloMsg(name));
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }


    /**
     * @param name The name of the user
     * @return "Hello <user name>!" text.
     */
    public String generateHelloMsg(final String name) {
        if ("".equals(name) || null == name) return "Hello!";

        return "Hello " + name + "!";
    }


}
