package com.hig.transitiondemo;

import junit.framework.TestCase;

/**
 * Test case for the MainActivity.
 *
 * Created by mariusz on 7/09/15.
 */
public class MainActivityTest extends TestCase {

    MainActivity act;

    @Override
    public void setUp() {
        act = new MainActivity();
    }

    @Override
    public void tearDown() {

    }


    public void testGeneratingHelloMsg() {
        assertEquals("Hello!", act.generateHelloMsg(""));
        assertEquals("Hello Mariusz!", act.generateHelloMsg("Mariusz"));
        assertEquals("Hello Andy!", act.generateHelloMsg("Andy"));
        assertEquals("Hello!", act.generateHelloMsg(null));
    }

    public void testDummy() {
        assertTrue(true);
    }

}
